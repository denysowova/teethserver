import os
from socketserver import TCPServer
from http.server import BaseHTTPRequestHandler
from recognition import replaceTeeth

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
PORT = 4321


class Server(TCPServer):
    allow_reuse_address = True


class RequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        if self.path == '/hello':
            replaceTeeth()
            imagePath = f"{__location__}/images/output/output.png"
            image = open(imagePath, 'rb')

            self.send_response(200)
            self.send_header('Content-type', 'image/png')
            self.end_headers()
            self.wfile.write(image.read())


with Server(("", PORT), RequestHandler) as httpd:
    print("serving at port", PORT)
    httpd.serve_forever()
