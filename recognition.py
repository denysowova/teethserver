import os
import cv2
import mediapipe as mp
from mediapipe.python.solutions.drawing_utils import _normalized_to_pixel_coordinates
from PIL import Image, ImageDraw

# current script location
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

teethImagePath = f"{__location__}/images/teeth/teeth.png"
imagePath = f"{__location__}/images/people/guy1.jpg"

# for drawing
lipsSet = frozenset([
                # Lips.
                (61, 146),
                (146, 91),
                (91, 181),
                (181, 84),
                (84, 17),
                (17, 314),
                (314, 405),
                (405, 321),
                (321, 375),
                (375, 291),
                (61, 185),
                (185, 40),
                (40, 39),
                (39, 37),
                (37, 0),
                (0, 267),
                (267, 269),
                (269, 270),
                (270, 409),
                (409, 291),
                (78, 95),
                (95, 88),
                (88, 178),
                (178, 87),
                (87, 14),
                (14, 317),
                (317, 402),
                (402, 318),
                (318, 324),
                (324, 308),
                (78, 191),
                (191, 80),
                (80, 81),
                (81, 82),
                (82, 13),
                (13, 312),
                (312, 311),
                (311, 310),
                (310, 415),
                (415, 308)])

# inner lips, closest indexes to the mouth center (best for closed mouth)
innerLipsCenterIndexes = [78, 191, 80, 81, 82, 13, 312, 311, 310, 415, 308, 324, 318, 402, 317, 14, 87, 178, 88, 95, 78]

# inner lips, closest indexes to the mouth center for the upper lip and middle indexes (a bit distant from center) of the lower lip. Best for most smiles
innerLipsNormalSmileLowerIndexes = [78, 191, 80, 81, 82, 13, 312, 311, 310, 415, 308, 325, 319, 403, 316, 15, 86, 179, 89, 96, 78]

# inner lips, middle indexes for both top and lower lips. Better for wider smiles
innerLipsWideSmileIndexes = [78, 183, 42, 41, 72, 11, 302, 303, 272, 407, 308, 325, 319, 403, 316, 15, 86, 179, 89, 96, 78]

outerLipsIndexes = [61, 185, 40, 39, 37, 0, 267, 269, 270, 409, 291, 375, 321, 405, 314, 17, 84, 181, 91, 146]

mp_drawing = mp.solutions.drawing_utils
mp_face_mesh = mp.solutions.face_mesh

# For static images:
drawing_spec = mp_drawing.DrawingSpec(thickness=1, circle_radius=1)
file_list = [imagePath]

def replaceTeeth():
    with mp_face_mesh.FaceMesh(
            static_image_mode=True,
            max_num_faces=1,
            min_detection_confidence=0.5) as face_mesh:

        for idx, file in enumerate(file_list):
            image = cv2.imread(file)
            # Convert the BGR image to RGB before processing.
            results = face_mesh.process(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))

            # Print and draw face mesh landmarks on the image.
            if not results.multi_face_landmarks:
                continue
            annotated_image = image.copy()
            for (index, face_landmarks) in enumerate(results.multi_face_landmarks):

                innerLipsPixels = []

                # convert points to pixels
                for i in innerLipsNormalSmileLowerIndexes:
                    pixel = _normalized_to_pixel_coordinates(face_landmarks.landmark[i].x,
                                                             face_landmarks.landmark[i].y,
                                                             image.shape[1],
                                                             image.shape[0])
                    innerLipsPixels.append(pixel)

                # draw detected points
                mp_drawing.draw_landmarks(
                    image=annotated_image,
                    landmark_list=face_landmarks,
                    connections=lipsSet,
                    landmark_drawing_spec=drawing_spec,
                    connection_drawing_spec=drawing_spec)
            cv2.imwrite('/tmp/annotated_image' + str(idx) + '.png', annotated_image)

            # original image with cropped mouth (draws transparent polygon)
            croppedImage = Image.open(imagePath).convert("RGBA")
            ImageDraw.Draw(croppedImage).polygon(innerLipsPixels, outline=(0, 0, 0, 0), fill=(0, 0, 0, 0))
            # croppedImage.show()

            # background image: either transparent or the original image itself (use the original to have empty spaces filled with the original teeth)
            # backgroundImage = Image.new('RGBA', croppedImage.size, (0, 0, 0, 180)) # transparent background
            backgroundImage = Image.open(imagePath).convert("RGBA")  # original image
            backgroundWidth, backgroundHeight = backgroundImage.size

            # minX = min(innerLipsPixels, key=lambda t: t[0])
            # maxX = max(innerLipsPixels, key=lambda t: t[0])
            # mouthWidth = maxX[0] - minX[0]

            # sort pixels by x value and get 4th value from beginning and end (teeth should be narrower than the cropped area)
            sortedInnerLipPixels = sorted(innerLipsPixels, key=lambda t: t[0])
            minX = sortedInnerLipPixels[3]
            maxX = sortedInnerLipPixels[len(sortedInnerLipPixels) - 3]
            mouthWidth = maxX[0] - minX[0]

            print("width", mouthWidth)

            minY = min(innerLipsPixels, key=lambda t: t[1])
            maxY = max(innerLipsPixels, key=lambda t: t[1])
            mouthHeight = maxY[1] - minY[1]

            print("height", mouthHeight)

            teethImage = Image.open(teethImagePath).convert("RGBA")
            resizedTeethImage = teethImage.resize((mouthWidth, mouthHeight), Image.ANTIALIAS)
            teethWidth, teethHeight = resizedTeethImage.size

            # origin of the teeth image to position correctly
            # offset = ((backgroundWidth - teethWidth) // 2, (backgroundHeight - teethHeight) // 2) # center in background
            offset = (minX[0], minY[1])

            backgroundImage.paste(resizedTeethImage, offset, resizedTeethImage)
            backgroundImage.paste(croppedImage, (0, 0), croppedImage)
            # backgroundImage.show()

            outputPath = f"{__location__}/images/output/output.png"
            os.makedirs(os.path.dirname(outputPath), exist_ok=True)

            backgroundImage.save(outputPath, "png")

